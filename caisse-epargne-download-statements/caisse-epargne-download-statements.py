#!/bin/env/python
# Python 3

from lxml import html as html_parse
import getpass
import logging
import random
import re
import requests
import sys
import urllib

log = logging.getLogger(__name__)

HOME_URL = 'https://www.caisse-epargne.fr/particuliers/ind_pauthpopup.aspx?srcurl=accueil'
USER_AGENT = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.65 Safari/537.36"


def error(code, msg):
    print(msg, file=sys.stderr)
    sys.exit(code)


def check(what, code, msg):
    if not what:
        error(code, msg)


def main(userid):
    check(userid.isnumeric(), 1, "Invalid customer identifier: must be numeric")

    s = requests.Session()
    s.headers['User-agent'] = USER_AGENT
    log.debug("[AUTH] [STEP 1] Load session")
    page = s.get(HOME_URL)

    html = html_parse.fromstring(page.content)
    html.make_links_absolute(HOME_URL)

    target = html.xpath('//a[contains(@id, "btnValider")]')[0].get('href').split("'")[1]

    form = html.forms[0]
    check(form.method == 'POST', 200, "Form method is not POST: API may have changed")
    inputs = dict(form.form_values())
    inputs['__EVENTTARGET'] = target
    inputs['__EVENTARGUMENT'] = ''
    for field in form.inputs:
        if field.value is None:
            inputs[field.name] = userid
            break
    else:
        error(201, "Form has no identifier field: API may have changed")

    log.debug("[AUTH] [STEP 2] Authenticate customer with %r", inputs.keys())

    page = s.post(form.action, data=inputs)
    check(page.ok, 202, "Authentication failed")

    html = html_parse.fromstring(page.content)
    html.make_links_absolute(page.url)
    form = html.forms[0]

    inputs = dict(form.form_values())
    for field in form.inputs:
        if field.type == 'password':
            inputs[field.name] = getpass.getpass("Customer password: ")
            break
    else:
        check(page.is_redirect, 203, "Unknown customer identifier")

    login_url = html.xpath('//a[contains(@id, "btnValider")]')[0].get('href').split("'")[1]
    log.debug("[AUTH] [STEP 4] Authenticate customer password")

    page = s.post(login_url, data=inputs, allow_redirects=False)
    check(page.ok, 2, "Wrong password")

    final_url = urllib.parse.urljoin(page.url, page.headers['location'])
    log.debug("[AUTH] [STEP 5] URL is %s", final_url)

    page = s.get(final_url, allow_redirects=False, headers={'Referer': HOME_URL})
    check(page.ok, 204, "Home page failed")

    html = html_parse.fromstring(page.content)
    who = html.xpath('//span[contains(@id, "lblUserConnecte")]')[0]

    print("Logged in as %s" % who.text.strip())

    base_data = dict(html.forms[0].form_values())
    base_data['__ASYNCPOST'] = 'true'
    log.debug("[GET ] Base data is %r", base_data)

    def request(target, argument, more=None, decode_ajax=True, **kwargs):
        data = base_data.copy()
        data['m_ScriptManager'] = 'MM$m_UpdatePanel|%s' % target
        data['__EVENTARGUMENT'] = argument
        data['__EVENTTARGET'] = target
        if more and isinstance(more, dict):
            data.update(more)
        log.debug("[GET ] Getting %s (%s): %r", target, argument, data)
        page = s.post(final_url, data=data, **kwargs)
        check(page.ok, 205, "Request failed for %s" % target)
        if decode_ajax and page.headers.get('content-type', '').lower().startswith('text/'):
            all_parts = page.content.decode('utf-8').split('|')
            parts = iter(all_parts)
            for part in parts:
                if part == 'hiddenField':
                    key = next(parts)
                    value = next(parts)
                    base_data[key] = value
            return all_parts[3]
        return page

    print("Retrieving statements")
    request('MM$m_PostBack', 'CPTSYNT1')
    statements = request('MM$Menu_Ajax', 'WNR12')
    html = html_parse.fromstring(statements)

    sel_accounts = html.xpath('//select[contains(@name, "cboClientsBancaires")]')[0]
    sel_years = html.xpath('//select[contains(@name, "cboConsultationAnnee")]')[0]

    # only one supported at the moment
    accountid = sel_accounts.value
    log.debug("[REL] Account id is %s", accountid)

    years = list(map(str.strip, sel_years.value_options))

    for year in years:
        print("Retrieving statements for year %s" % year)
        statements = request(sel_years.name, '', more={sel_years.name: year, sel_accounts.name: accountid})
        html = html_parse.fromstring(statements)

        for month in html.xpath('//a[contains(@id, "MM_CONSULTATION_NUMERISATION_ReleveRepeater")]'):
            mname = month.find('span').text
            log.debug("[REL] Month %s", mname)
            target = month.get('href').split('"')[1]
            pdf = request(target, '', more={sel_years.name: year, sel_accounts.name: accountid}, decode_ajax=False, stream=True)
            check("application/pdf" in pdf.headers.get('content-type').lower(), 206, "Statement is not a PDF file")
            with open('%s-%s.pdf' % (accountid, '-'.join(reversed(mname.split('/')))), 'wb') as f:
                for chunck in pdf.iter_content(2048):
                    f.write(chunck)
                print("Downloaded %s successfully" % f.name)


if __name__ == '__main__':
    logging.basicConfig(level=logging.WARNING)
    main(input("Customer code: "))
